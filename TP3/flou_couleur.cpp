// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

function OCTET* flou(OCTET *image){
  for (int i=1; i < nH-1; i++)
   for (int j=1; j < nW-1; j++)
     {
       ImgOut[i*nW+j]= (ImgIn[i*nW+j]+ImgIn[(i-1)*nW+j]+ImgIn[(i+1)*nW+j]+ImgIn[i*nW+(j-1)]+ImgIn[i*nW+(j+1)]+ImgIn[(i-1)*nW+(j-1)]+ImgIn[(i-1)*nW+(j+1)]+ImgIn[(i+1)*nW+(j-1)]+ImgIn[(i+1)*nW+(j+1)])/9;

     }
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, S;

  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   //sscanf (argv[3],"%d",&S);

   OCTET *ImgIn, *ImgOut, *ImgR, *ImgB, *ImgV ;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
   int tab_couleur[nTaille*3];

  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
   allocation_tableau(ImgR, OCTET, nTaille);
   allocation_tableau(ImgB, OCTET, nTaille);
   allocation_tableau(ImgV, OCTET, nTaille);

	
   //   for (int i=0; i < nTaille; i++)
   // {
   //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
   //  }

planR(ImgR, ImgIn,nTaille);
planV(ImgV, ImgIn,nTaille);
planB(ImgB, ImgIn,nTaille);


 for (int i=0; i < (nTaille*3); i+=3)
     {
       tab_couleur[i]=R[i/3];
       tab_couleur[i+1]=G[i/3];
       tab_couleur[i+2]=B[i/3];

     }

    for(int j=0; j<nTaille*3; j+=3){
      ImgOut[j]=(tab_couleur[j]+tab_couleur[j+1]+tab_couleur[j+2])/3;
    }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
} 