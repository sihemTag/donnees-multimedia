#include <stdio.h>

#include "image_ppm.h"



int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);


   OCTET *ImgIn, *ImgOut , *tabR , *tabfr , *tabV,*tabB,*tabfv,*tabfb;
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);
   //-----------------------------------
  allocation_tableau(tabfr, OCTET,nTaille);
    allocation_tableau(tabR, OCTET,nTaille);
   planR( tabR, ImgIn , nTaille ) ; 
   //-----------------------------------------------------
     allocation_tableau(tabfb, OCTET,nTaille);
    allocation_tableau(tabB, OCTET,nTaille);
   planB( tabB, ImgIn ,nTaille ) ; 
   //----------------------------------------
     allocation_tableau(tabfv, OCTET,nTaille);
    allocation_tableau(tabV, OCTET,nTaille);
   planV( tabV, ImgIn  ,nTaille ) ; 
	
   //   for (int i=0; i < nTaille; i++)
   // {
   //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
   //  }

//8 voisins 

for( int i = 0; i<nH;i++)
   {for (int j =0 ; j<nW ; j++)
      { int sr =0,sv=0 ,sb= 0 ;
       if ( i == 0 ||j==0 || i ==nH-1 ||  j == nW-1)
        {  
           tabfb[i*nW+j] = tabB[i*nW+j]; 
           tabfr[i*nW+j] = tabR[i*nW+j]; 
           tabfv[i*nW+j] = tabV[i*nW+j]; 
         }
         else { 
             for (int k= i-1; k<=i+1;k++)   
            { 
            for (int l=j-1;l<=j+1 ; l++)  {   sr = tabR[k*nW+l ]+sr ;        sv = tabV[k*nW+l ]+sv ;    sb = tabB[k*nW+l ]+sb ;                      }
            }
           
          tabfr[i*nW+j]=sr/9 ; 
          tabfv[i*nW+j]=sv/9 ;
          tabfb[i*nW+j]=sb/9 ;

              }
  }
   }


  
for (int i = 0 ; i<nTaille ; i++ ) 
   {
        ImgOut[3*i]=    tabfr[i];
        ImgOut[3*i+1]= tabfv[i];
        ImgOut[3*i+2]= tabfb[i]; 
        
   }
   


     ecrire_image_ppm(cNomImgEcrite,ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);


   return 1;

 }