#include <stdio.h>
#include "image_ppm.h"
#include<fstream>

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille,indice,c;
  
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   //sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[2],"%d",&c);
   sscanf (argv[3],"%d",&indice);
   //sscanf (argv[5],"%d",&S);
   

   OCTET *ImgIn, *ImgOut;
   
   int tab[256];
   std::ofstream mon_fichier("profil.dat");
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   //   for (int i=0; i < nTaille; i++)
   // {
   //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
   //  }

for (int i=0; i<=256; i++){
tab[i]=0;
}
 

 if (c==0) {
  for (int i=0; i<nW; i++){
    tab[ImgIn[indice*nH+i]]++;
  }

 }

if (c==1) {
  for (int i=0; i<nH-1; i++){
    tab[ImgIn[i*nW+indice]]++;
  }

 }

     
     for (int i=0;i<256;i++){
     printf("%d %d\n ",i,tab[i] );}

   //ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}

