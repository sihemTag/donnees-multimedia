#include <stdio.h>
#include "image_ppm.h"
#include<fstream>

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, S;
  
  if (argc != 2) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
  // sscanf (argv[2],"%s",cNomImgEcrite);
   //sscanf (argv[2],"%d",&S);

   OCTET *ImgIn, *ImgOut;
   
   int tab1[256],tab2[256],tab3[256]  ;
   std::ofstream mon_fichier("histo.dat");
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   //   for (int i=0; i < nTaille; i++)
   // {
   //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
   //  }

for (int i=0; i<=256; i++){
tab1[i]=0;
tab2[i]=0;
tab3[i]=0;
}

     
     for (int i=0;i<nH; i++){
     for (int j=0; j<nW; j++){
     tab1[ImgIn[i*nW+j]]++ ;
     tab2[ImgIn[(i+1)*nW+j]]++ ;
     tab3[ImgIn[(i+2)*nW+j]]++ ;
   }}
     
     for (int i=0;i<256;i++){
     mon_fichier<<i<<" "<<tab1[i]<<" "<<tab2[i]<<" "<<tab3[i]<<"\n";}

   //ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
