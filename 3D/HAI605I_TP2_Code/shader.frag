// --------------------------------------------------------------------------
// gMini,
// a minimal Glut/OpenGL app to extend                              
//
// Copyright(C) 2007-2009                
// Tamy Boubekeur
//                                                                            
// All rights reserved.                                                       
//                                                                            
// This program is free software; you can redistribute it and/or modify       
// it under the terms of the GNU General Public License as published by       
// the Free Software Foundation; either version 2 of the License, or          
// (at your option) any later version.                                        
//                                                                            
// This program is distributed in the hope that it will be useful,            
// but WITHOUT ANY WARRANTY; without even the implied warranty of             
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
// GNU General Public License (http://www.gnu.org/licenses/gpl.txt)           
// for more details.                                                          
//                                                                          
// --------------------------------------------------------------------------

uniform float ambientRef;
uniform float diffuseRef;
uniform float specRef;
uniform float shininess;

varying vec4 p;
varying vec3 n;

void main (void) {
     vec3 P = vec3 (gl_ModelViewMatrix * p); //Position du point à éclairer
     vec3 N = normalize (gl_NormalMatrix * n); //Normal en ce point
     vec3 V = normalize (-P); //Vecteur de vue

     
    
    vec4 lightAmbiant = ambientRef * gl_LightModel.ambient*gl_FrontMaterial.ambient ;  //reflexion ambiante
    //vec4 lightDiff = diffuseRef * gl_LightSource[0].diffuse *gl_FrontMaterial.diffuse * dot(L,N);  //reflexion diffuse
    //vec4 lightSpec = specRef * gl_LightSource[0].specular *gl_FrontMaterial.specular * pow(dot(R,V),shininess);  //reflexion speculaire

    for(int i=0;i<3;i++){
        vec3 L = normalize(gl_LightSource[i].position.xyz-P);  //direction de la lumiere
        vec3 R = (2. * dot(N,L) * N)-L;  //direction de la lumiere apres rebond sur la surface
        float x = max(dot(L,N),0.0);
        vec4 lightDiff = diffuseRef * gl_LightSource[i].diffuse *gl_FrontMaterial.diffuse * x;  //reflexion diffuse
        lightAmbiant += lightDiff;
        float y = max(dot(R,V),0.0);
        vec4 lightSpec = specRef * gl_LightSource[i].specular *gl_FrontMaterial.specular * pow(y,shininess);  //reflexion speculaire
        lightAmbiant += lightSpec;

    }

    ////////////////////////////////////////////////
    //Eclairage de Phong à calculer en utilisant
    ///////////////////////////////////////////////
    // gl_LightSource[i].position.xyz Position de la lumière i
    // gl_LightSource[i].diffuse Couleur diffuse de la lumière i
    // gl_LightSource[i].specular Couleur speculaire de la lumière i
    // gl_FrontMaterial.diffuse Matériaux diffus de l'objet
    // gl_FrontMaterial.specular Matériaux speculaire de l'objet

    //+vec4 (lightDiff.xyz, 1)+ vec4 (lightSpec.xyz,1)

    gl_FragColor =vec4 (lightAmbiant.xyz, 1);
}
 
