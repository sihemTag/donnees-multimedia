#include <stdio.h>
#include "image_ppm.h"
#include<fstream>

int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH, nW, nTaille,indice,c;

  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   //sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[2],"%d",&c);
   sscanf (argv[3],"%d",&indice);
    
   

   OCTET *ImgIn;
   
   
   std::ofstream mon_fichier("profil.dat");
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;

   int tab[nW];

   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   

for (int i=0; i<=nW; i++){
tab[i]=0;
}
 
 if (c==0) {
  for (int i=0; i<nW-1; i++){
    tab[i]=ImgIn[indice*nW+i] ;
  }

 }

if (c==1) {
  for (int i=0; i<nH-1; i++){
    tab[i]=ImgIn[i*nW+indice];
  }

 }

     
     for (int i=0;i<nW;i++){
     mon_fichier<<i<<" "<<tab[i]<<"\n";}

   //ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);

   return 1;
}

