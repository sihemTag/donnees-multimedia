#include <stdio.h>
#include "image_ppm.h"
#include <bits/stdc++.h>

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille,sb,sh;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&sb);
   sscanf (argv[4],"%d",&sh);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);


for (int i=1; i < nH-1; i++)
   for (int j=1; j < nW-1; j++)
     {
       if (ImgIn[i*nW+j]<=sb) ImgOut[i*nW+j]=0;
       if (ImgIn[i*nW+j]>=sh) ImgOut[i*nW+j]=255;
       if ( (ImgIn[i*nW+j]<=sb) && (ImgIn[i*nW+j]>=sh)) {
           if (ImgIn[(i-1)*nW+(j-1)]==255) {ImgOut[i*nW+j]=255;}
           else if (ImgIn[(i-1)*nW+j]==255) {ImgOut[i*nW+j]=255;}
           else if (ImgIn[(i+1)*nW+(j+1)]==255) {ImgOut[i*nW+j]=255;} 
           else if (ImgIn[i*nW+(j-1)]==255) {ImgOut[i*nW+j]=255;}
           else if (ImgIn[i*nW+(j+1)]==255) {ImgOut[i*nW+j]=255;}
           else if (ImgIn[(i+1)*nW+(j-1)]==255) {ImgOut[i*nW+j]=255;}
           else if (ImgIn[(i+1)*nW+j]==255) {ImgOut[i*nW+j]=255;}
           else if (ImgIn[(i+1)*nW+(j+1)]==255) {ImgOut[i*nW+j]=255;}
           else {ImgOut[i*nW+j]=0;}
       }
     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}