// test_couleur.cpp : Seuille une image couleur 

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250],cNomImgBase[250];
  int nH, nW, nTaille, nR, nG, nB;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgBase);



   OCTET *ImgIn, *ImgOut, *ImgBase;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   lire_nb_lignes_colonnes_image_ppm(cNomImgBase, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);

   allocation_tableau(ImgBase, OCTET, nTaille3);
   lire_image_pgm(cNomImgBase, ImgBase, nH * nW);

   allocation_tableau(ImgOut, OCTET, nTaille3);
	

     int erreur=0;
     for(int i=0; i<nW; i++){
         for (int j=0; j<nH; j++){
             erreur+= pow( (ImgIn[i*nW+j]- ImgBase[i*nW+j]),2 );
         }
     }
     erreur=erreur/(nW*nH);
     printf("%d\n",erreur);

   //ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   return 1;
}