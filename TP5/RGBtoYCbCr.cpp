#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250],cNomImgEcrite2[250];
  int nH, nW, nTaille, nR, nG, nB;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%s",cNomImgEcrite2);



   OCTET *ImgIn, *ImgOut, *ImgOut2;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
   allocation_tableau(ImgOut2, OCTET, nTaille);

  
	
   for (int i=0; i < nTaille3; i+=3)
     {
       nR = ImgIn[i];
       nG = ImgIn[i+1];
       nB = ImgIn[i+2];
       ImgOut[i/3]= -0.1687*nR - 0.3313*nG + 0.5*nB + 128;
       /*ImgOut[i+1]=-0.1687*nR - 0.3313*nG + 0.5*nB + 128;
       ImgOut[i+2]=-0.1687*nR - 0.3313*nG + 0.5*nB + 128;*/

       ImgOut2[i/3]= 0.5*nR - 0.4187*nG - 0.0813*nB + 128;
       /*ImgOut2[i+1]=0.5*nR - 0.4187*nG - 0.0813*nB + 128;
       ImgOut2[i+2]=0.5*nR - 0.4187*nG - 0.0813*nB + 128;*/

     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   ecrire_image_pgm(cNomImgEcrite2, ImgOut2,  nH, nW);
   free(ImgIn);
   return 1;
}
