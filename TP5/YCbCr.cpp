#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLueY[250], cNomImgEcrite[250],cNomImgLueCB[250], cNomImgLueCR[250];
  int nH, nW, nTaille;
  
  if (argc != 5) 
     {
       printf("Usage: ImageInY.ppm ImageInCb.ppm ImageInCr.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLueY) ;
   sscanf (argv[2],"%s",cNomImgLueCB) ;
   sscanf (argv[3],"%s",cNomImgLueCR) ;
   sscanf (argv[4],"%s",cNomImgEcrite);
  
   OCTET *ImgInY, *ImgOut,*ImgInCB, *ImgInCR ;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLueY, &nH, &nW);
   lire_nb_lignes_colonnes_image_pgm(cNomImgLueCB, &nH, &nW);
   lire_nb_lignes_colonnes_image_pgm(cNomImgLueCR, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgInY, OCTET, nTaille);
   lire_image_pgm(cNomImgLueY, ImgInY, nH * nW);

   allocation_tableau(ImgInCB, OCTET, nTaille);
   lire_image_pgm(cNomImgLueCB, ImgInCB, nH * nW);

   allocation_tableau(ImgInCR, OCTET, nTaille);
   lire_image_pgm(cNomImgLueCR, ImgInCR, nH * nW);


   allocation_tableau(ImgOut, OCTET, nTaille3);
  
	
   for (int i=0; i < nH; i++){ 
   for (int j=0; j < nW; j++)
     { 
       int r = ImgInY[i*nW+j] + 1.402*(ImgInCR[i*nW+j] - 128);
       int g = ImgInY[i*nW+j] - 0.34414*(ImgInCB[i*nW+j] - 128) - 0.714414*(ImgInCR[i*nW+j] - 128);
       int b = ImgInY[i*nW+j] + 1.772*(ImgInCB[i*nW+j] - 128);
    
       ImgOut[3*(i*nW+j)]= r;
       ImgOut[3*(i*nW+j)+1]=g;
       ImgOut[3*(i*nW+j)+2]= b;
    
       if (ImgOut[3*(i*nW+j)]<=0) {ImgOut[3*(i*nW+j)]=0;}
       if (ImgOut[3*(i*nW+j)]>=255) {ImgOut[3*(i*nW+j)]=255;}
      
       if (ImgOut[3*(i*nW+j)+1]<=0) {ImgOut[3*(i*nW+j)+1]=0;}
       if (ImgOut[3*(i*nW+j)+1]>=255) {ImgOut[3*(i*nW+j)+1]=255;}

       if (ImgOut[3*(i*nW+j)+2]<=0) {ImgOut[3*(i*nW+j)+2]=0;}
       if (ImgOut[3*(i*nW+j)+2]>=255) {ImgOut[3*(i*nW+j)+2]=255;}

     }
   } 

       


   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgInY);
   free(ImgInCB);
   free(ImgInCR);
   return 1;
}
